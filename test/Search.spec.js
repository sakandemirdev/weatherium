import { shallowMount, mount } from '@vue/test-utils'
import Search from '@/components/Search.vue'

describe('Search', () => {
  test('is a Vue instance', () => {
    const wrapper = shallowMount(Search)
    expect(wrapper.vm).toBeTruthy()
  })

  it('updates searchText on input', async () => {
    const wrapper = mount(Search);
    const newValue = 'Istanbul'
    const textInput = wrapper.find('input')
    await textInput.setValue(newValue)

    expect(wrapper.vm.searchText).toBe(newValue);
  })

  it('search button is disabled when input empty', async () => {
    const wrapper = mount(Search);
    const newValue = null
    const textInput = wrapper.find('input')
    await textInput.setValue(newValue)
    const searchButton = wrapper.find('button')

    expect(searchButton.element.disabled).toBe(true);
  })

  it('search button is active when input is not empty', async () => {
    const wrapper = mount(Search);
    const newValue = 'Istanbul'
    const textInput = wrapper.find('input')
    await textInput.setValue(newValue)
    const searchButton = wrapper.find('button')

    expect(searchButton.element.disabled).toBe(false);
  })

  it('emit search when button clicked', async () => {
    const wrapper = mount(Search);
    const newValue = 'Istanbul'
    const textInput = wrapper.find('input')
    await textInput.setValue(newValue)
    const searchButton = wrapper.find('button')

    searchButton.element.click();
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().search[0]).toEqual([newValue])
  })
})
