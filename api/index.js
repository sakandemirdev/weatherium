export default ({ $axios }) => {
  return {
    getCurrentWeatherByCity({cityName}){
      return $axios.get('/weather', { params: {q: cityName }})
    },
    getDailyWeatherByCity({cityName}){
      return $axios.get('/forecast', { params: {q: cityName}})
    }
  }
}
