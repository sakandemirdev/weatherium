import api from '../api/index.js'

export default function (ctx, inject) {
  inject('api', api(ctx))
}
