import Vue from 'vue'

Vue.filter('convertToCelcius', val => `${Math.round(val - 273.15)}°`)
