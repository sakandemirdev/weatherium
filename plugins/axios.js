import Vue from 'vue'
export default function ({ $axios, $notify, redirect, app, store, ...params }) {
  $axios.onRequest((config) => {
    config.params = {
      ...config.params,
      appid: 'f875057a188ace7686783ab370e5eb1d'
    }
    return config
  })

  $axios.onError((error) => {
    const res = error.response.data
    Vue.notify({
      type: 'error',
      group: 'error',
      text: res.message.toUpperCase()
    })
  })
}
