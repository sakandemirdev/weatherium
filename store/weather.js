export const state = () => ({
  weatherList: [],
  defaultCities: [
    'Columbia Falls',
    'Chandler',
    'New York',
  ]
})

export const mutations = {
  ADD_WEATHER_TO_WEATHER_LIST(state, weather) {
    if(state.weatherList.some(item => item.name === weather.name)) return;
    state.weatherList.unshift(weather)
  },
}

export const actions = {
   getWeatherByCityName ({ commit }, { cityName }) {
      return this.$api.getCurrentWeatherByCity({cityName}).then((res) => {
        commit('ADD_WEATHER_TO_WEATHER_LIST', res.data)
        return res
      })
  },
  getDefaultCitiesWeathers ({ commit, dispatch, state }) {
    const cities = state.defaultCities
    const requests = []

    cities.forEach(city => {
      const method = () => dispatch('getWeatherByCityName',{cityName: city})
      requests.push(method)
    })

    return Promise.all(requests.map(item => item()))
  },
  getDailyWeatherByCity ({ commit }, { cityName, count }){
     return this.$api.getDailyWeatherByCity({ cityName, count })
  }
}
