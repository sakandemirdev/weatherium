export const state = () => ({
  headerTitle: null,
})

export const mutations = {
  SET_HEADER_TITLE(state, title) {
    state.headerTitle = title
  },
}

export const actions = {
  setHeaderTitle ({ commit }, title) {
    commit('SET_HEADER_TITLE', title)
  },
}
